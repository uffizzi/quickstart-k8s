# Uffizzi Quickstart for Kubernetes (~ 1 minute)

Go from pull request to Uffizzi Ephemeral Environment in less than one minute. This quickstart will create a virtual Kubernetes cluster on Uffizzi Cloud and deploy a sample microservices application from this repository. Once created, you can connect to the cluster with the Uffizzi CLI, then manage the cluster via `kubectl`, `kustomize`, `helm`, and other tools. You can clean up the cluster by closing the pull request or manually deleting it via the Uffizzi CLI.
This example also uses the reusable Uffizzi gitlab workflow at https://gitlab.com/uffizzi/cluster-action. 
Do checkout the `.gitlab-ci.yml` file for more details on usage.

### 1. Fork this `quickstart-k8s` repo

[https://gitlab.com/uffizzi/quickstart-k8s](https://gitlab.com/uffizzi/quickstart-k8s)

### 2. Open a pull request for the `try-uffizzi` branch against `main` in your fork

⚠️ Be sure that you're opening a PR on the branches of _your fork_ of this repository.

![img.png](misc/location.png)

Once you open a PR you should see that the services for the application being deployed to the cluster.
### 3. View cluster details and access the service endpoints

Once the action is finished running, check the gitlab runner logs for the cluster details.

Once you open the PR you should be able to see this section before the PR comments which will have a link to the pipeline which
is creating the virtual cluster environment.

![img.png](misc/pipeline location.png)

Once you go to the link above you should be able to see stages of the pipeline.
Choose the uffizzi_cluster_deploy stage and click on the logs to see the details of the cluster deployed.
Choose the kustomize_apply stage and click on the logs to see the details of the services deployed.

![img.png](misc/build steps.png)

Below are the logs for the cluster
![img.png](misc/cluster logs.png)

And these are the logs for the services
![img.png](misc/services logs.png)

___
## What to expect

The PR will trigger a [Gitlab pipeline](.gitlab-ci.yml) that uses the Uffizzi CLI, and Kubernetes manifests to create a Uffizzi Ephemeral Environment for the [microservices application](#architecture-of-this-example-app) defined by this repo. When the workflow completes, the Ephemeral Environment URL will be posted in the logs of the pipeline.

## How it works

### Configuration

Ephemeral Environments are configured with [Kubernetes manifests](manifests/) that describe the application components and a [Gitlab pipeline](.gitlab-ci.yml) that includes a series of jobs triggered by a `pull_request` event and subsequent `push` events:

1. Build and push the voting-app images
2. Create the Uffizzi cluster using the uffizzi-cli
3. Apply the Kubernetes manifests to deploy the application to the Uffizzi cluster
4. Delete the Ephemeral Environment when the PR is merged/closed or after

<!-- ### Uffizzi Cloud

Running this workflow will create a [Uffizzi Cloud](https://uffizzi.com) account and project from your Gitlab user and repo information, respectively. If you sign in to the [Uffizzi Dashboard](https://app.uffizzi.com/sign_in), you can view logs, password protect your Ephemeral Environments, manage projects and team members, set role-based access controls, and configure single-sign-on (SSO).

Open-source projects preview for free on Uffizzi Cloud. All other accounts can subscribe to our Starter or Pro plans. See [our pricing](https://uffizzi.com/pricing) for details. If you're an open-source maintainer, you can request free access by sending an email to opensource@uffizzi.com. Alternatively, if you don't want to use Uffizzi Cloud, you can [install open-source Uffizzi](https://github.com/UffizziCloud/uffizzi_app/blob/develop/INSTALL.md) on your own Kubernetes cluster. -->

## Acceptable Use

We strive to keep Uffizzi Cloud free or inexpensive for individuals and small teams. Therefore, activities such as crypto mining, file sharing, bots, and similar uses that lead to increased costs and intermittent issues for other users are strictly prohibited per the [Acceptable Use Policy](https://www.uffizzi.com/legal/acceptable-use-policy). Violators of this policy are subject to permanent ban.

## Architecture of this Example App

The application defined by this repo allows users to vote for dogs or cats and see the results. It consists of the following microservices:

<img src="https://user-images.githubusercontent.com/7218230/192601868-562b705f-bf39-4eb8-a554-2a0738bd8ecf.png" width="400">

* `voting` - A frontend web app in [Python](/vote) that lets you vote between two options
* `redis` - A [Redis](https://hub.docker.com/_/redis/) queue that collects new votes
* `worker` - A [.NET Core](/worker/src/Worker) worker that consumes votes and stores them in...
* `db` - A [PostgreSQL](https://hub.docker.com/_/postgres/) database backed by a Docker volume
* `result` - A [Node.js](/result) web app that shows the results of the voting in real time
